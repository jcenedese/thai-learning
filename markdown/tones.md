| Initial Consonant Class | Vowel Duration | "Live" or "Dead" Ending |  Tone |
|------------------------|----------------|------------------------|---------|
| Low                    | Short          | Dead/None              | High    |
| Low                    | Long           | Dead                   | Falling |
| Low                    | Any            | Live                   | Mid     |
| Middle                 | Any            | Live                   | Mid     |
| Middle                 | Any            | Dead                   | Low     |
| High                   | Any            | Live                   | Rising  |
| High                   | Any            | Dead                   | Low     |