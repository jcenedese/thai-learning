take this markdown table as data, you have to show me the data contained in the first column of a random row, and you have to make me guess the value of the corresponding 3 other columns of the row
only ever show me the data of the first column for the guess. Always keep me guessing, no need to ask

| Thai Consonant | Class | Initial Sound | Final Sound |
| -------------- | ----- | ------------- | ----------- |
| ค              | Low   | kh            | k           |
| ฆ              | Low   | kh            | k           |
| ง              | Low   | ng            | ng          |
| ช              | Low   | ch            | t           |
| ซ              | Low   | s             | t           |
| ฌ              | Low   | ch            | t           |
| ญ              | Low   | y             | n           |
| ฑ              | Low   | th            | t           |
| ฒ              | Low   | th            | t           |
| ณ              | Low   | n             | n           |
| ท              | Low   | th            | t           |
| ธ              | Low   | th            | t           |
| น              | Low   | n             | n           |
| พ              | Low   | ph            | p           |
| ฟ              | Low   | f             | p           |
| ภ              | Low   | ph            | p           |
| ม              | Low   | m             | m           |
| ย              | Low   | y             | [vowel]     |
| ร              | Low   | r             | n           |
| ล              | Low   | l             | n           |
| ว              | Low   | w             | [vowel]     |
| ฬ              | Low   | l             | n           |
| ฮ              | Low   | h             | -           |
| ก              | Mid   | g             | k           |
| จ              | Mid   | j             | t           |
| ฎ              | Mid   | d             | t           |
| ฏ              | Mid   | dt            | t           |
| ด              | Mid   | d             | t           |
| ต              | Mid   | dt            | t           |
| บ              | Mid   | b             | p           |
| ป              | Mid   | bp            | p           |
| อ              | Mid   | [silent]      | [vowel]     |
| ข              | High  | kh            | k           |
| ฉ              | High  | ch            | -           |
| ฐ              | High  | th            | t           |
| ถ              | High  | th            | t           |
| ผ              | High  | ph            | -           |
| ฝ              | High  | f             | -           |
| ศ              | High  | s             | t           |
| ษ              | High  | s             | t           |
| ส              | High  | s             | t           |
| ห              | High  | h             | -           |
