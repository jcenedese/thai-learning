
### Low Class Consonants



| Thai Consonant | Initial Sound | Final Sound
| ----------------- | ------------------------ | -------------------- |
| <span style="font-size:1.5em;">ค</span> | kh | k
| <span style="font-size:1.5em;background-color:yellow;">ฆ</span> | kh | k
| <span style="font-size:1.5em;">ง</span> | ng | ng
| <span style="font-size:1.5em;">ช</span> | ch | t
| <span style="font-size:1.5em;">ซ</span> | s | t
| <span style="font-size:1.5em;background-color:red;">ฌ</span> | ch | t
| <span style="font-size:1.5em;">ญ</span> | y | n
| <span style="font-size:1.5em;">ฑ</span> | th | t
| <span style="font-size:1.5em;background-color:red;">ฒ</span> | th | t
| <span style="font-size:1.5em;">ณ</span> | n | n
| <span style="font-size:1.5em;">ท</span> | th | t
| <span style="font-size:1.5em;">ธ</span> | th | t
| <span style="font-size:1.5em;">น</span> | n | n
| <span style="font-size:1.5em;">พ</span> | ph | p
| <span style="font-size:1.5em;">ฟ</span> | f | p
| <span style="font-size:1.5em;">ภ</span> | ph | p
| <span style="font-size:1.5em;">ม</span> | m | m
| <span style="font-size:1.5em;">ย</span> | y | [vowel]
| <span style="font-size:1.5em;">ร</span> | r | n
| <span style="font-size:1.5em;">ล</span> | l | n
| <span style="font-size:1.5em;">ว</span> | w | [vowel]
| <span style="font-size:1.5em;background-color:yellow;">ฬ</span> | l | n
| <span style="font-size:1.5em;background-color:yellow;">ฮ</span> | h | -


### Middle Class Consonants

| <span style="font-size:1.5em;">Thai Consonant</span> | <span style="font-size:1.5em;">Initial Sound</span> | <span style="font-size:1.5em;">Final Sound</span> |
| ----------------- | ------------------------ | -------------------- |
| <span style="font-size:1.5em;">ก</span> | g | k |
| <span style="font-size:1.5em;">จ</span> | j | t |
| <span style="font-size:1.5em;background-color:yellow;">ฎ</span> | d | t |
| <span style="font-size:1.5em;background-color:yellow;">ฏ</span> | dt | t |
| <span style="font-size:1.5em;">ด</span> | d | t |
| <span style="font-size:1.5em;">ต</span> | dt | t |
| <span style="font-size:1.5em;">บ</span> | b | p |
| <span style="font-size:1.5em;">ป</span> | bp | p |
| <span style="font-size:1.5em;">อ</span> | [silent] | [vowel] |

### High Class Consonants

| <span style="font-size:1.5em;">Thai Consonant</span> | <span style="font-size:1.5em;">Initial Sound</span> | <span style="font-size:1.5em;">Final Sound</span> |
| ----------------- | ------------------------ | -------------------- |
| <span style="font-size:1.5em;">ข</span> | kh | k |
| <span style="font-size:1.5em;">ฉ</span> | ch | - |
| <span style="font-size:1.5em;background-color:yellow;">ฐ</span> | th | t |
| <span style="font-size:1.5em;">ถ</span> | th | t |
| <span style="font-size:1.5em;">ผ</span> | ph | - |
| <span style="font-size:1.5em;">ฝ</span> | f | - |
| <span style="font-size:1.5em;">ศ</span> | s | t |
| <span style="font-size:1.5em;">ษ</span> | s | t |
| <span style="font-size:1.5em;">ส</span> | s | t |
| <span style="font-size:1.5em;">ห</span> | h | - |